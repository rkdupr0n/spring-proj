package com.dr0hith.client1;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.bind.annotation.GetMapping;

@SpringBootApplication
@RestController
public class Client1Application {

	@Autowired
	private RestTemplate template;

	@Value("${master.provider.url}")
	private String url;

	@GetMapping(value = "/getService")
	List<String> getService() {
		//TODO: Cast this as a valid List<String>
		return template.getForObject(url, List.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(Client1Application.class, args);
	}

	@Bean
	public RestTemplate template() {
		return new RestTemplate();
	}
}
