package com.dr0hith.microservice1;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;

@SpringBootApplication
@RestController
@RequestMapping("/master-service")
public class Microservice1Application {

	@GetMapping("/getAllServices")
	public List<String> getAllServices() {
		return Stream.of("Premium", "Legendary", "Godly").collect(Collectors.toList());
	}

	public static void main(String[] args) {
		SpringApplication.run(Microservice1Application.class, args);
	}

}
